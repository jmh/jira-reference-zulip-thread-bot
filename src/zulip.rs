use std::io::{self, ErrorKind};
use websocket::{ClientBuilder, Message, sync::Client as WebSocketClient};
use websocket::sync::stream::Stream;

const ZULIP_API_URL: &str = "wss://comco.zulipchat.com/api/v1/events";

pub struct ZulipClient<S>
    where
        S: Stream,
{
    client: WebSocketClient<S>,
}

impl<S: io::Write + io::Read> ZulipClient<S> {
    pub fn new() -> Result<Self, io::Error> {
        // Connect to the Zulip WebSocket API
        let client = ClientBuilder::new(ZULIP_API_URL)
            .unwrap()
            .connect_insecure()
            .map_err(|err| {
                eprintln!("Failed to connect to Zulip WebSocket API: {}", err);
                io::Error::new(ErrorKind::Other, "Connection error")
            })?;

        println!("Connected to Zulip WebSocket API");

        Ok(Self { client })
    }

    pub fn authenticate(&mut self, api_key: &str, email: &str) -> Result<(), io::Error> {
        // Authenticate with Zulip
        let auth_message = json!({
            "type": "auth",
            "api_key": api_key,
            "email": email
        });
        self.client.send_message(&Message::text(auth_message.to_string()))?;
        println!("Authenticated with Zulip");
        Ok(())
    }

    pub fn subscribe(&mut self) -> Result<(), io::Error> {
        // Subscribe to events
        let subscribe_message = json!({
            "type": "subscribe",
            "event_types": ["message"]
        });
        self.client.send_message(&Message::text(subscribe_message.to_string()))?;
        println!("Subscribed to message events");
        Ok(())
    }

    pub fn receive_messages(&mut self) -> Result<(), io::Error> {
        // Start receiving and handling events
        loop {
            let message = self.client.recv_message()?;
            match message {
                Message::Text(text) => {
                    println!("Received message: {}", text);
                    // Handle the received message here
                }
                _ => {
                    eprintln!("Received unsupported message type");
                }
            }
        }
    }
}


