mod jira;
//mod zulip;

use std::collections::HashMap;
use std::process::exit;
use log::{debug, error};
use reqwest::{Client, Response};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use tokio::sync::mpsc;
use tokio::time::{self, Duration};


const ZULIP_API_URL: &str = "https://comco.zulipchat.com/api/v1";
const ZULIP_API_KEY: &str = "";

#[derive(Debug, Deserialize)]
struct Event {
    id: i64,
    type_str: String,
    message: Option<Message>,
}

#[derive(Debug, Deserialize)]
struct Message {
    content: String,
    sender_email: String,
    stream_id: i64,
}

#[derive(Debug, Deserialize)]
struct InitialState {
    result: String,
    queue_id: String,
    last_event_id: i64,
    event_queue_longpoll_timeout_seconds: u16,
}

#[derive(Serialize)]
struct ZulipAuth {
    email: String,
    api_key: String,
}

async fn initialise_events(client: &Client, auth: &ZulipAuth) -> Result<Response, reqwest::Error> {
    let url = ZULIP_API_URL.to_owned() + "/register";
    debug!("Connecting to: {}", url);
    //let message = r#"event_types=["message"]"#;
    let message: Vec<(&str, Vec<&str>)> = vec![("event_types", vec!["message"])];
    client.post(url)
        .basic_auth(&auth.email, Some(&auth.api_key))
        .json(&message)
        .send()
        .await
}

async fn get_events(client: &Client, auth: &ZulipAuth, queue_id: &str) -> Result<Response, reqwest::Error> {
    let url = ZULIP_API_URL.to_owned() + "/events";
    let q_id_str = format!(r#"queue_id={}"#, queue_id);
    let q_id_vec: Vec<(&str, &str)> = vec![("queue_id", queue_id)];
    debug!("Connecting to: {}", url);
    client.post(url)
        .basic_auth(&auth.email, Some(&auth.api_key))
        //.form(&q_id_str)
        .form(&q_id_vec)
        .send()
        .await
}

async fn receive_events(mut rx: mpsc::Receiver<Event>) {
    while let Some(event) = rx.recv().await {
        if let Some(message) = event.message {
            println!(
                "Stream ID: {}, Sender: {}, Message: {}",
                message.stream_id, message.sender_email, message.content
            );
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let client = Client::new();
    let auth = ZulipAuth {
        email: "zeta-bot@comco.zulipchat.com".to_string(),
        api_key: ZULIP_API_KEY.to_string(),
    };

    let res = initialise_events(&client, &auth).await?;

    if res.status() != 200 {
        error!("Error from the Zulip API: {:?}", &res.status());
        error!("Initial response text: {:?}", &res.text().await?); // Print response for debugging
        exit(2)
    } else {
    }

    let res_text = res.text().await.expect("Unable to get text from HTTP request");
    std::fs::write("/tmp/result.json", &res_text)
        .expect("Unable to write file");
    let initial_state: InitialState = serde_json::from_str(&res_text)?;

    let queue_id = initial_state.queue_id;
    let poll_timeout = initial_state.event_queue_longpoll_timeout_seconds;
    let mut last_event_id = initial_state.last_event_id;
    let rx = mpsc::channel::<Event>(100);

    tokio::spawn(receive_events(rx.1));

    loop {
        let res = get_events(&client, &auth, &queue_id).await.expect("Could not get events");
        let res_text = res.text().await?;
        println!("Event response text: {}", &res_text); // Print response for debugging
        std::fs::write("/tmp/event.json", &res_text).expect("Unable to write file");
        let events: Vec<Event> = serde_json::from_str(&res_text)?;

        for event in events {
            if event.id > last_event_id {
                last_event_id = event.id;
                if let Err(_) = rx.0.send(event).await {
                    println!("Error sending event to receiver");
                }
            }
        }

        time::sleep(Duration::from_secs(poll_timeout as u64)).await;
    }
}