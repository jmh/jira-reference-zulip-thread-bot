use tungstenite::{connect, Message};
use url::Url;

const ZULIP_REALTIME_WEBSOCKET_URL: &str = "wss://comco.zulipchat.com/api/v1/events";
const BOT_EMAIL_ADDRESS: &str = "zeta-bot@comco.zulipchat.com";
const BOT_API_KEY: &str = "";

async fn connect_to_zulip() {
    let (mut socket, _) = connect(Url::parse(ZULIP_REALTIME_WEBSOCKET_URL).expect("unable to parse URL"))
        .expect("Failed to connect to Zulip Realtime API");

    let auth_message = format!(
        "{{\"type\": \"auth\", \"api_key\": \"{}\", \"email\": \"{}\"}}",
        BOT_API_KEY, BOT_EMAIL_ADDRESS
    );
    socket
        .write(Message::Text(auth_message))
        .expect("Failed to send authentication message");

    // Subscribe to message events
    let subscribe_message = r#"{"type": "subscribe", "event_types": ["message"]} "#;
    socket
        .write(Message::Text(subscribe_message.to_string()))
        .expect("Failed to send subscribe message");

    // Listen for incoming messages
    loop {
        if let Ok(msg) = socket.read() {
            match msg {
                Message::Text(text) => {
                    println!("Received message: {}", text);
                }
                _ => {}
            }
        }
    }
}

#[tokio::main]
async fn main() {
    connect_to_zulip().await;
}