use super::comment_create::Author;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
struct Struct3 {
    pub text: String,
    pub r#type: String,
}

#[derive(Debug, Deserialize)]
struct Struct2 {
    pub content: Vec<Struct3>,
    pub r#type: String,
}

#[derive(Debug, Deserialize)]
struct Body {
    pub content: Vec<Struct2>,
    pub r#type: String,
    pub version: i64,
}

#[derive(Debug, Deserialize)]
struct AvatarUrls {
    pub _16x16: String,
    pub _24x24: String,
    pub _32x32: String,
    pub _48x48: String,
}

#[derive(Debug, Deserialize)]
struct Struct1 {
    pub account_id: String,
    pub account_type: String,
    pub active: bool,
    pub avatar_urls: AvatarUrls,
    pub display_name: String,
    #[serde(rename="self")]
    pub r#orig_self: String,
    pub time_zone: String,
    pub email_address: Option<String>,
}

#[derive(Debug, Deserialize)]
struct Struct {
    pub author: Author,
    pub body: Body,
    pub created: String,
    pub id: String,
    pub jsd_public: bool,
    #[serde(rename="self")]
    pub r#orig_self: String,
    pub update_author: Struct1,
    pub updated: String,
}

#[derive(Debug, Deserialize)]
struct Root {
    pub comments: Vec<Struct>,
    pub max_results: i64,
    pub start_at: i64,
    pub total: i64,
}