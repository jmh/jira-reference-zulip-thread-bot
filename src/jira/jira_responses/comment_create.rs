
use serde::{Deserialize};

#[derive(Debug, Deserialize)]
pub struct Line {
    pub text: String,
    pub r#type: String,
}

#[derive(Debug, Deserialize)]
pub struct Paragraph {
    pub content: Vec<Line>,
    pub r#type: String,
}

#[derive(Debug, Deserialize)]
pub struct Body {
    pub content: Vec<Paragraph>,
    pub r#type: String,
    pub version: i64,
}

#[derive(Debug, Deserialize)]
pub struct AvatarUrls {
    pub _16x16: String,
    pub _24x24: String,
    pub _32x32: String,
    pub _48x48: String,
}

#[derive(Debug, Deserialize)]
pub struct Author {
    pub account_id: String,
    pub account_type: String,
    pub active: bool,
    pub avatar_urls: AvatarUrls,
    pub display_name: String,
    pub email_address: String,
    #[serde(rename="self")]
    pub r#orig_self: String,
    pub time_zone: String,
}

#[derive(Debug, Deserialize)]
pub struct Resp {
    pub author: Author,
    pub body: Body,
    pub created: String,
    pub id: String,
    pub jsd_public: bool,
    #[serde(rename="self")]
    pub r#orig_self: String,
    pub update_author: Author,
    pub updated: String,
}